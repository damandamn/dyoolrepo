﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageBuilder : MonoBehaviour {

    public Material platMat;

	// Use this for initialization
	void Start () {
        ConstructFlat();
        
	}

    public void ConstructFlat()
    {
        GameObject plat1 = GameObject.CreatePrimitive(PrimitiveType.Cube);
        plat1.transform.position = new Vector3(0, -5, 0);
        plat1.transform.localScale = new Vector3(25, 2, 1);
        plat1.GetComponent<Renderer>().material = platMat;


        StageData.activePlatforms.Add(new Platform(-2.25F, plat1));
    }

}
