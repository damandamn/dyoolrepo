﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGroundHandler : MonoBehaviour {

    public Platform nextPlat;
    public float platHeight;

    bool grounded = true;
    public Collider col;

    private void Start()
    {
        col = GetComponent<BoxCollider>();
    }

    private void Update()
    {
        nextPlat = GetCurrentPlat();
        if (nextPlat != null)
        {
            platHeight = nextPlat.height;
        } else
        {
            platHeight = -8;
        }
        
    }

    public Platform GetCurrentPlat()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position - new Vector3(0, 1.5F), Vector3.down);
        if (Physics.Raycast(ray, out hit, 200))
        {
            GameObject go = hit.collider.gameObject;

            if (go.tag != "Player" && go.tag != "Hitbox")
            {
                foreach (Platform p in StageData.activePlatforms)
                {
                    if (p.plat == go)
                    {
                        return p;
                    }
                }
            }
        }

        return null;
    }


}
